package com.example.ecommercespring.repostory;

import com.example.ecommercespring.entity.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShopRepository extends JpaRepository<Shop, String> {
}
