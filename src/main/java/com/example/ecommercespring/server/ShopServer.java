package com.example.ecommercespring.server;

import com.example.ecommercespring.entity.ResponseObject;
import com.example.ecommercespring.entity.Shop;
import com.example.ecommercespring.repostory.ShopRepository;
import com.example.ecommercespring.utils.Constant;
import com.example.ecommercespring.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShopServer {
    @Autowired
    ShopRepository shopRepository;

    public ResponseObject saveShop(Shop obj) {
        obj.setCreated(DateUtil.getTimeLongCurrent());
        obj.setUpdated(DateUtil.getTimeLongCurrent());
        obj.setStatus(Shop.ShopStatus.ACTIVE);
        return new ResponseObject(Constant.STATUS_ACTION_SUCCESS, "thành công", shopRepository.save(obj));
    }

    public ResponseObject getAllShop() {
       List<Shop> result = shopRepository.findAll();
       return new ResponseObject(Constant.STATUS_ACTION_SUCCESS,"thành công", result);
    }
}
