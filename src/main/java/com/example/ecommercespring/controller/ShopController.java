package com.example.ecommercespring.controller;

import com.example.ecommercespring.entity.ResponseObject;
import com.example.ecommercespring.entity.Shop;
import com.example.ecommercespring.repostory.ShopRepository;
import com.example.ecommercespring.server.ShopServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@CrossOrigin
@RequestMapping("/api/v1/shop")
public class ShopController {
    @Autowired
    ShopServer shopServer;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<ResponseObject> getAll(){
        ResponseObject result = shopServer.getAllShop();
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<ResponseObject> create(@RequestBody Shop shop){
        ResponseObject result = shopServer.saveShop(shop);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
