package com.example.ecommercespring.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nameCategory;
    private Long created;
    private Long updated;
    private CategoryStatus status;
    public enum CategoryStatus{
        ACTIVE, DISABLED, DEACTIVATED
    }
}
