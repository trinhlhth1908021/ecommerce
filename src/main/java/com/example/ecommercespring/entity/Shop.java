package com.example.ecommercespring.entity;

import com.example.ecommercespring.utils.DateUtil;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import javax.validation.constraints.Size;
import javax.persistence.*;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "user_account")
public class Shop {
    @Id
    @GeneratedValue(generator = "shop_generator")
    @GenericGenerator(
            name = "shop_generator",
            strategy = "com.example.ecommercespring.config.ShopGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String id;
    @Size(min = 5, message = "Minimum name length: 5 characters")
    private String shopName;
    @Size(min = 5, max = 255)
    private String shopDescription;
    private String shopAddress;
    private Long created;
    private Long updated;
    private ShopStatus status;
    public enum ShopStatus{
        ACTIVE, DISABLED, DEACTIVATED
    }
}
