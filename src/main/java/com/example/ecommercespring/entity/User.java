package com.example.ecommercespring.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "user_account")
@ToString
public class User {
    @Id
    @GeneratedValue(generator = "user_generator")
    @GenericGenerator(
            name = "user_generator",
            strategy = "com.example.ecommercespring.config.UserGenerator"
    )
    private String id;
    @Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
    @Column(unique = true, nullable = false, updatable = false)
    private String username;
    @Column(nullable = false)
    private String email;
    private String password;
    private String phone;
    @Column(nullable = false)
    private String fullName;
    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles;
    private Double balance;
    private Long createAt;
    private Long updateAt;
    private UserStatus status;
    private String thumbnail;
    public enum UserStatus{
        ACTIVE, DISABLED, DEACTIVATED
    }

}
