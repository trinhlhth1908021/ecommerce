package com.example.ecommercespring.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(generator = "product_generator")
    @GenericGenerator(
            name = "product_generator",
            strategy = "com.example.ecommercespring.config.ProductGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String id;
    private String productName;
    private String productDescription;
    private Double productPrice;
    private ProductStatus status;
    public enum ProductStatus{
        ACTIVE, DISABLED, DEACTIVATED
    }

}
